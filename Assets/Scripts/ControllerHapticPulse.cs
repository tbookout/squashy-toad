﻿namespace VRTK
{

    using UnityEngine;
    using System.Collections;

    public class ControllerHapticPulse : MonoBehaviour
    {

        private VRTK_ControllerActions controllerActions;

        // Use this for initialization
        void Start()
        {
            controllerActions = GetComponent<VRTK_ControllerActions>();

        }


        public void TriggerPulse(int pulseStrength)
        {
            controllerActions.TriggerHapticPulse((ushort)Mathf.Clamp(pulseStrength, 0, 3999));
        }
    }

}