﻿using UnityEngine;
using System.Collections;
using VRTK;


public class FrogControl : MonoBehaviour {

    public GameObject m_Frog;
    private FrogController m_FrogController;

    void Start ()
    {
        m_FrogController = m_Frog.GetComponent<FrogController>();

        //Setup controller event listeners
        if (GetComponent<VRTK_ControllerEvents>() == null)
        {
            Debug.LogError("VRTK_ControllerEvents is required to be attached to a Controller that has the FrogControl script attached to it");
        }
        else
        {
            GetComponent<VRTK_ControllerEvents>().TriggerPressed += new ControllerInteractionEventHandler(DoTriggerPressed);
        }
    }


    private void DoTriggerPressed(object sender, ControllerInteractionEventArgs e)
    {
        m_FrogController.Jump();
    }


}
