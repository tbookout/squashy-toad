﻿using UnityEngine;
using System.Collections;
using VRTK;

public class FrogController : MonoBehaviour {

    public float[] jumpStrength;
    public float jumpAngle;
    public float limitForwardSpeed;
   
    private Rigidbody my_Rigidbody;
    private AudioSource m_AudioSource;
    //private bool onGround = true;
    private int collisionGroundCount;
    private Transform m_Camera;
    private float limitForwardSpeedSqr;
    private int jumpCount;
    private GameObject m_GameOverUI;


    void Start ()
    {
        my_Rigidbody = GetComponent<Rigidbody>();
        my_Rigidbody.isKinematic = false;
        my_Rigidbody.detectCollisions = true;

        m_AudioSource = GetComponent<AudioSource>();
        m_GameOverUI = transform.Find("UiPivot").gameObject;

        //GameObject[] GObjects;
        //GObjects = GameObject.FindGameObjectsWithTag("MainCamera");
        //foreach (GameObject gObj in GObjects)
        //{
        //    //if(gObj.name == "Camera (eye)") m_Camera = gObj;
        //    if (gObj.name == "Camera") m_Camera = gObj;
        //}

        m_Camera = VRTK.VRTK_DeviceFinder.HeadsetCamera();


        limitForwardSpeedSqr = limitForwardSpeed * limitForwardSpeed;
        collisionGroundCount = 0;
        jumpCount = 0;

        m_GameOverUI.SetActive(false);

        //Cursor.lockState = CursorLockMode.Locked;
    }

    
    void Update()
    {        
        if (Input.GetKeyDown(KeyCode.Space))
        {           
            Jump();
        }        
    }


    public void Jump()
    { 
        //Debug.DrawRay(transform.position, (Vector3.down * distToGroundChk), Color.red);
        
        // Equivalent to: rigidbody.velocity.magnitude < maxForwardSpeed, but faster.
        if ((jumpCount < jumpStrength.Length) && (my_Rigidbody.velocity.sqrMagnitude < limitForwardSpeedSqr))
        {
            m_AudioSource.Play();

            var jumpVector = (Vector3.ProjectOnPlane(m_Camera.forward, Vector3.up).normalized * jumpStrength[jumpCount]);
            jumpVector = Vector3.RotateTowards(jumpVector, Vector3.up, (jumpAngle * Mathf.Deg2Rad), 0.0F);

            my_Rigidbody.AddForce(jumpVector, ForceMode.VelocityChange);
            jumpCount++;
        }
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            collisionGroundCount++;
            if (collisionGroundCount > 0)
            {
                //onGround = true;
                jumpCount = 0;
            }
                
        }

        if (collision.gameObject.tag == "Lethal")
        {
            Debug.Log("You are dead");

            // Disable Jumping & RigidBody response to collision
            jumpCount = jumpStrength.Length;  
            my_Rigidbody.isKinematic = true;
            my_Rigidbody.detectCollisions = false;

            m_GameOverUI.SetActive(true);
        }

    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            collisionGroundCount--;
            //if (collisionGroundCount < 1)
            //    onGround = false;
        }
    }

}
