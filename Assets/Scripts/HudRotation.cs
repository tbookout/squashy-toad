﻿using UnityEngine;
using System.Collections;

public class HudRotation : MonoBehaviour {
    
    private Transform m_Camera;


    // Use this for initialization
    void Start ()
    {
        //GameObject[] GObjects;
        //GObjects = GameObject.FindGameObjectsWithTag("MainCamera");
        //foreach (GameObject gObj in GObjects)
        //{
        //    if (gObj.name == "Camera (eye)") m_Camera = gObj;
        //}

        m_Camera = VRTK.VRTK_DeviceFinder.HeadsetCamera();
    }
	

	// Update is called once per frame
	void Update ()
    {
        var lookVector = Vector3.ProjectOnPlane(m_Camera.forward, Vector3.up) ;
        transform.rotation = Quaternion.LookRotation(lookVector);
    }

}
