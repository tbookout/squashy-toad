﻿using UnityEngine;
using System.Collections;

public class LaneEnd_Collider : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        Destroy(other.GetComponentInParent<VehicleMovement>().gameObject);
    }

}
