﻿using UnityEngine;
using System.Collections;

enum LaneType { Safe, Danger };


public class LaneSpawner : MonoBehaviour {

    public GameObject laneStart;
    public GameObject[] lanePrefabsSafe;
    public GameObject[] lanePrefabsDanger;
    public float laneSpawnDistance;
    public GameObject player;

    [Tooltip("Percentage chance of multiple Safe Lanes spawning")]
    public float chanceSafeLanes;  

    private LaneType lastLaneType = LaneType.Safe;
    private float laneSpawnOffset = 1000f;


    // Use this for initialization
    void Start ()
    {
        //First lane is pre-configured
        var lane = Instantiate(laneStart);
        lane.transform.parent = transform;   
    }

    void Update ()
    {
        while (laneSpawnOffset < (laneSpawnDistance + player.transform.position.z))
        {
            CreateRandomLane(laneSpawnOffset);
            laneSpawnOffset += 1000f;

            foreach (Transform child in transform)
            {
                Debug.Log(child.name + " " + child.position.z);

                if (child.position.z < (player.transform.position.z - laneSpawnDistance))
                    GameObject.Destroy(child.gameObject);
            }
        }
    }


    private void CreateRandomLane(float offset)
    {
        GameObject lane;

        if (lastLaneType == LaneType.Safe && (chanceSafeLanes <= Random.value) )
        {
            int lanePrefab = Random.Range(0, lanePrefabsDanger.Length);
            lane = Instantiate(lanePrefabsDanger[lanePrefab]);
            lastLaneType = LaneType.Danger;
        }
        else
        {
            int lanePrefab = Random.Range(0, lanePrefabsSafe.Length);
            lane = Instantiate(lanePrefabsSafe[lanePrefab]);
            lastLaneType = LaneType.Safe;
        }

        lane.transform.SetParent(transform, false);
        lane.transform.Translate(0, 0, offset);
    }

}
