﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public float secondsTillLevelLoad = 0f;

    // Use this for initialization
    void Start () {


    }
	

	// Update is called once per frame
	void Update () {

        //if (Input.GetKeyDown(KeyCode.Space)) { LoadNextScene(); }

        if (secondsTillLevelLoad  > 0f )
        {
            secondsTillLevelLoad -= Time.deltaTime;

            if (secondsTillLevelLoad <= 0f)
            {
                LoadNextScene();
            }
        }          

    }

    public void LoadPrevScene()
    {

        int levelIdx = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(levelIdx - 1);

        //int levelIdx = SceneManager.GetActiveScene().buildIndex;

        //// If last scene, return to Main Menu
        //if (levelIdx == (SceneManager.sceneCountInBuildSettings - 1))
        //{
        //    SceneManager.LoadScene(0);
        //}
        //else
        //{
        //    SceneManager.LoadScene(levelIdx + 1);
        //}
    }

    public void LoadNextScene()
    {
        int levelIdx = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(levelIdx + 1);                
    }

    public void LoadCurrentScene()
    {
        int levelIdx = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(levelIdx);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
