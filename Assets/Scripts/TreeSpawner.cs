﻿using UnityEngine;
using System.Collections;

public class TreeSpawner : MonoBehaviour {

    public GameObject treePrefab;
    public int minTreeCnt;
    public int maxTreeCnt;
    public float minTreeScale;
    public float maxTreeScale;


    void Start()
    {
        SpawnTrees();
    }


    void SpawnTrees ()
    {
        int treeSpawnCnt = Random.Range(minTreeCnt, maxTreeCnt);
        for (int icnt = 0; icnt < treeSpawnCnt; icnt++)
        {
            float x_Offset = Random.Range(-45f, 45f);
            float z_Offset = Random.Range(-4f, 4f);
            float treeScale = Random.Range(minTreeScale, maxTreeScale);

            var tree = Instantiate(treePrefab);
            tree.transform.parent = transform;            
            tree.transform.localPosition= new Vector3(x_Offset, 0f, z_Offset);                        
            tree.transform.localScale = new Vector3(treeScale, treeScale, treeScale);
        } 
    }
}
