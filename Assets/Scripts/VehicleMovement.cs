﻿using UnityEngine;
using System.Collections;

public class VehicleMovement : MonoBehaviour {

    public float vehicleVelocity;

    public Rigidbody vehicleRigidbody;

    // Use this for initialization
    void Start ()
    {
        vehicleRigidbody = GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	//void Update ()
 //   {
 //       transform.Translate(-vehicleVelocity * Time.deltaTime, 0, 0);	
	//}


    void FixedUpdate()
    {
        vehicleRigidbody.MovePosition(transform.position + Vector3.right * vehicleVelocity * Time.deltaTime);
    }



}

