﻿using UnityEngine;
using System.Collections;

public class VehicleSpawner : MonoBehaviour {

    public GameObject spawnPrefab;
    public float spawnMinTime = 1.5f;
    public float spawnMeanTime = 10f;
    
    private float nextSpawnTime = 0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Time.time > nextSpawnTime)
        {
            Spawn();

            //nextSpawnTime = Time.time + (Random.Range(minspawnInterval, maxspawnInterval));
            nextSpawnTime = Time.time + spawnMinTime +  (-Mathf.Log(Random.value) * spawnMeanTime);
            
        }
	
	}

    void Spawn()
    {
        //public static Object Instantiate(Object original, Vector3 position, Quaternion rotation, Transform parent);
        Instantiate(spawnPrefab, transform.position, transform.rotation, transform);
    }

}
