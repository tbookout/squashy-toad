# README #

Squashy Toad course for HTC VIVE

### What is this repository for? ###

* This is the Squashy Toad implementation for the Udemy course "Make VR Games in Unity with C#"
* Specifically it is the Section 4 Beta content, modified to include HTC Vive support.  
[Make VR Games in Unity with C# - Section: 4](https://www.udemy.com/vrcourse/learn/v4/content)

### How do I get set up? ###

* Requires SteamVR and VRTK

**SteamVR**

* Import the [SteamVR Plugin](https://www.assetstore.unity3d.com/en/#!/content/32647) from the Unity Asset Store

**VRTK**

* [*Preferred Method*]  Clone the repository [VRTK GitHub](https://github.com/thestonefox/VRTK.git)
* [*Simple Method*]  Import the [VRTK](https://www.assetstore.unity3d.com/en/#!/content/64131) from the Unity Asset Store


### Latest Build ###
[SquashyToad.zip](https://bitbucket.org/tbookout/squashy-toad/downloads/SquashyToad.zip)